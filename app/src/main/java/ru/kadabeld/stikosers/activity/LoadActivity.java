package ru.kadabeld.stikosers.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import ru.kadabeld.stikosers.R;

public class LoadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);
    }
}
